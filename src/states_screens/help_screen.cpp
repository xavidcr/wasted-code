//  SuperTuxKart - a fun racing game with go-kart
//  Copyright (C) 2009-2015 Marianne Gagnon
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#include "states_screens/help_screen.hpp"

#include "io/file_manager.hpp"


#include "challenges/unlock_manager.hpp"
#include "config/player_manager.hpp"
#include "config/user_config.hpp"
#include "guiengine/widget.hpp"
#include "guiengine/widgets/button_widget.hpp"
#include "guiengine/widgets/list_widget.hpp"
#include "guiengine/widgets/ribbon_widget.hpp"
#include "input/device_manager.hpp"
#include "input/input_manager.hpp"
#include "input/keyboard_device.hpp"
#include "karts/kart_properties_manager.hpp"
#include "race/race_manager.hpp"

#include "states_screens/state_manager.hpp"

#include <IXMLReader.h>

#include "wchar.h"

#include "utils/ptr_vector.hpp"

using namespace GUIEngine;
using namespace io;

std::unordered_map<std::string, const char*> test_help_pages = {
    {"page1", "help1.stkgui"},
    {"page2", "help2.stkgui"},
    {"page3", "help3.stkgui"},
    {"page4", "help4.stkgui"},
    {"page5", "help5.stkgui"},
    {"page6", "help6.stkgui"}
};

HelpScreen::HelpScreen() : Screen("helpbase.stkgui")
{
    m_resizable = false;
    setPages(&test_help_pages);
    m_current_page = m_pages["page1"];
}   // HelpScreen

const char* HelpScreen::getPage(const char* pageName)
{
    return m_pages[pageName];
} // getPage

void HelpScreen::setPages(std::unordered_map<std::string, const char*>* new_pages_ptr)
{
    m_pages = *new_pages_ptr;
} // setPages

void HelpScreen::renderPage()
{
    const std::string& filename = std::string(m_current_page);
    std::string path = file_manager->getAssetChecked(FileManager::GUI_SCREEN, filename, true);
    IXMLReader* xml = file_manager->createXMLReader( path );

    while(xml && xml->read())
    {
        if(wcscmp(L"vertical-tabs", xml->getNodeName()) == 0)
        {
            m_menu_block->getChildren().clearAndDeleteAll();
            parseScreenFileDiv(xml, m_menu_block->getChildren(), m_menu_block->getIrrlichtElement());
        }
        else if(wcscmp(L"main-block", xml->getNodeName()) == 0)
        {
            Log::warn("help page", "I deleted this and added more \n");
            m_main_block->getChildren().clearAndDeleteAll();
            parseScreenFileDiv(xml, m_main_block->getChildren(), m_main_block->getIrrlichtElement());
        }
    }

    delete xml;
    calculateLayout();
} // renderPage


// -----------------------------------------------------------------------------

void HelpScreen::loadedFromFile()
{
    m_menu_block = this->getWidget<RibbonWidget>(XML_NODE_MENU_ID);
    m_main_block = this->getWidget<Widget>(XML_NODE_CONTENT_ID);
    if(m_menu_block == NULL)
    {
        Log::error("help page", "Missing Node type %s \n", XML_NODE_MENU_ID);
        return;
    }
    else if (m_menu_block == NULL)
    {
        Log::error("help page", "Missing Node type %s \n", XML_NODE_CONTENT_ID);
        return;
    }
    renderPage();
}   // loadedFromFile

// -----------------------------------------------------------------------------

void HelpScreen::eventCallback(Widget* widget, const std::string& name, const int playerID)
{
    if (name == "category")
    {
        std::string selection = ((RibbonWidget*)widget)->getSelectionIDString(PLAYER_ID_GAME_MASTER);
        if (!selection.empty())
        {
            m_current_page = m_pages[selection];
        }

        if(m_current_page)
        {
            renderPage();
            StateManager::get()->reshowTopMostMenu();
        }
    }
    else if (name == "back")
    {
        StateManager::get()->escapePressed();
    }
}   // eventCallback

// -----------------------------------------------------------------------------

void HelpScreen::init()
{
    Screen::init();

    RibbonWidget* nav = ((RibbonWidget*)m_menu_block);

    if (nav != nullptr)
    {
        nav->setFocusForPlayer(PLAYER_ID_GAME_MASTER);
        nav->select( m_current_page, PLAYER_ID_GAME_MASTER );
    }

}  //init



// -----------------------------------------------------------------------------
