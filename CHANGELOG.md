# Changelog
This file documents notable changes to Wasted across versions since its inception.

It should be kept in mind that some versions have a less complete changelog than others, and that this changelog do not list the details of the many small bugfixes and improvements which together make a significant part of the progress between releases.

For similar reasons, and because some features are vastly more complex than others, attributions of main changes should not be taken as a shortcut for overall contribution.

## Wasted 0.3.0-alpha (31. December 2023) - (Almost) Happy new Year!

With this release we added a ton of **things**!
We also had our first real test session with 4 players that actually allowed me to fix some nasty bugs.

* Fixed smoke not being removed after car is fixed if car has health < 25 #52
* Fixed driving backwards gives other players HPs #60
* Reassign cycle/reset camera buttons for gamepads to avoid error during compilation (f1414eb011)
* Added a Windows build! (Thanks Chris for the help) #56
* Add script to facilitate creation of portable windows build
* Format numbers properly in final score screen #57
* Add podium at the end of the match the shows the first 3 players in order 
* Cars were only represented with default color on the podium #59
* Allow players to fetch LAN servers if not logged in, WAN servers if logged in with a STK account (Will be removed in the future)
* Fix wrong height for name and HP of the player
* Fix ordering of in-game players #58
* Some code cleanup related to #48

**SFX**

* Added new car fix sfx
* Added new zombie powerup sfk
* Replaced menu car selection sfx
* Replaced menu start race sfx

**TRACKS**

* Added new default free roam map (Insert author here)
* Added podium to default map

## Wasted 0.2.0-alpha (01. October 2023)

We are happy to announce the second Wasted release!
This release is also dedicated to SpongyCake. He won't be actively contributing anymore but we thank him for all the huge work done so far.
Hope to see your contributions again someday!

This release has huge changes! I am having trouble writing this changelog so something may be missing :D

* Separated FFA server code from client code - spongycake (b5e125e241)
* Fixed resized window camera rotation bug - Rampoina (3acf6e0b83)
* Huge changes to score system. Now calculated adding HPs to played time
* Free camera mode with camera rotation and in-game camera change!
* New version of 173-rx-mk2 car - Crystaldaevee
* Players eliminated by falling down are not entirely removed from the game - franzbonanza (d68596ec23)
* Add smoke effect to players with damaged cars (<25 HPs) - franzbonanza (8f3159303a)
* Eliminate people who are AFK (don't kick them) - franzbonanza (d704aab76a)
* Fix explosions for everybody playing (previously they were buggy)
* Don't remove cars when they explode in the map
* Fixed spectator camera not being assigned automatically to alive players
* Fix camera rotation not being calculated properly - franzbonanza (bb4568b98a)
* Add button to reset camera position and rotation - franzbonanza (a24969fda2)
* Changed how damages are handled: now we also check for car class (medium, light or heavy) - franzbonanza(a09e7f6dd2)
* Fix car preview in menu - franzbonanza (92a99eba89)
* Change make variables - spongycake (e96e44d8ad)
* Add  camera zoom in/out - franzbonanza (4d8cd3e432)
* Start tutorial precondition gui_tour - spongycake (5404cb4c9f)

### Non-game related:

* Documentation updates


## Wasted 0.1.0-alpha (18. May 2023)

We are Happy to announce our first alpha release!
With this release the game finally reached a playing state.

* added 3 official cars(173-rx-mk2, 222RS, blade-xtr) and 1 official map (sky grid)
* add derby game mode
* add damage system
* changed music and added music playlists to game maps
* add server filter to show only compatible servers
* changed menu UI to access to online matches easily
* 6 new powerups
* added new help manual

### Non-game related:

* new official master server up to easily host matches at any time
* added appimage script to automatically create appimages and improved linux builder script
* server protocol version set to 0


