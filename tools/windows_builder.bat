REM CREATE THE FOLDER FOR THE WINDOWS RELEASE
mkdir ..\..\wasted_portable_winx86_64

REM COPY WASTED-ASSETS FOLDER
robocopy  ..\..\wasted-assets ..\..\wasted_portable_winx86_64\wasted-assets\ /E

REM COPY WASTED BUILD FILES
robocopy ..\..\wasted-code\data ..\..\wasted_portable_winx86_64\wasted-code\data /E
robocopy ..\..\wasted-code\build\bin\Debug\ ..\..\wasted_portable_winx86_64\wasted-code\build-x86_64\bin /E
copy .\run_game.bat ..\..\wasted_portable_winx86_64\
ren ..\..\wasted_portable_winx86_64\wasted-code\build-x86_64\bin\supertuxkart.exe wasted.exe
