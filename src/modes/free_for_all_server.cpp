//  SuperTuxKart - a fun racing game with go-kart
//  Copyright (C) 2018 SuperTuxKart-Team
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#include "modes/free_for_all_server.hpp"
#include "graphics/camera.hpp"
#include "graphics/explosion.hpp"
#include "graphics/irr_driver.hpp"
#include "graphics/render_info.hpp"
#include "items/projectile_manager.hpp"
#include "karts/explosion_animation.hpp"
#include "karts/abstract_kart.hpp"
#include "karts/controller/controller.hpp"
#include "karts/kart_properties.hpp"
#include "network/network_config.hpp"
#include "network/network_string.hpp"
#include "network/protocols/game_events_protocol.hpp"
#include "network/stk_host.hpp"
#include "tracks/track.hpp"
#include "utils/string_utils.hpp"
#include "utils/translation.hpp"

#include <algorithm>
#include <utility>
#include <iostream>


// ----------------------------------------------------------------------------
/** Constructor. Sets up the clock mode etc.
 */
FreeForAllServer::FreeForAllServer() : FreeForAll()
{
    if (RaceManager::get()->hasTimeTarget())
    {
        WorldStatus::setClockMode(WorldStatus::CLOCK_COUNTDOWN,
                                  RaceManager::get()->getTimeTarget());
    }
    else
    {
        WorldStatus::setClockMode(CLOCK_CHRONO);
    }
}   // FreeForAll

// ----------------------------------------------------------------------------
FreeForAllServer::~FreeForAllServer()
{
}   // ~FreeForAll

// ----------------------------------------------------------------------------
void FreeForAllServer::init()
{
    Log::verbose("FreeForAll", "I am a client with server capabilities.");
    FreeForAll::init();
    m_display_rank = false;
    m_count_down_reached_zero = false;
    m_use_highscores = false;

}   // init

// ----------------------------------------------------------------------------
/** Called when a battle is restarted.
 */
void FreeForAllServer::reset(bool restart)
{
    FreeForAll::reset(restart);
    m_count_down_reached_zero = false;
    if (RaceManager::get()->hasTimeTarget())
    {
        WorldStatus::setClockMode(WorldStatus::CLOCK_COUNTDOWN,
                                  RaceManager::get()->getTimeTarget());
    }
    else
    {
        WorldStatus::setClockMode(CLOCK_CHRONO);
    }
    m_scores.clear();
    m_scores.resize(m_karts.size(), 100);
}   // reset

// ----------------------------------------------------------------------------
/** Called when the match time ends.
 */
void FreeForAllServer::countdownReachedZero()
{
    for(unsigned int i = 1; i < getNumKarts() ; i++){
        if(!getKartAtPosition(i)->hasFinishedRace()){
            getKartAtPosition(i)->finishedRace(RaceManager::get()->getTimeTarget());
        }
    }
    // Prevent negative time in network soccer when finishing
    m_time_ticks = 0;
    m_time = 0.0f;
    m_count_down_reached_zero = true;
}   // countdownReachedZero

// ----------------------------------------------------------------------------
/** Called when a kart is hit.
 *  \param kart_id The world kart id of the kart that was hit.
 *  \param hitter The world kart id of the kart who hit(-1 if none).
 */
bool FreeForAllServer::setKartHpsPowerup(int kart_id, int hpAmount)
{
    if (NetworkConfig::get()->isNetworking() &&
            NetworkConfig::get()->isClient())
        return false;

    if (isRaceOver())
        return false;

    setKartHps(kart_id, hpAmount);
    return true;
}

// ----------------------------------------------------------------------------
/** Called when the score of kart needs updated.
 *  \param kart_id The world kart id of the kart that was hit.
 *  \param hitter The world kart id of the kart who hit(-1 if none).
 */
void FreeForAllServer::setKartHps(int kart_id, int hpAmount)
{
    m_karts[kart_id]->setHp(hpAmount);
    calculateScore(kart_id);
}   // handleScoreInServer
// ----------------------------------------------------------------------------
/** Called when a kart is hit.
 *  \param kart_id The world kart id of the kart that was hit.
 *  \param hitter The world kart id of the kart who hit(-1 if none).
 */
bool FreeForAllServer::kartHit(int kart_id, int hitter)
{
    if (NetworkConfig::get()->isNetworking() &&
            NetworkConfig::get()->isClient())
        return false;

    if (isRaceOver())
        return false;

    handleKartHitInServer(kart_id, hitter);
    return true;
}   // kartHit

// ----------------------------------------------------------------------------
/** Called when the score of kart needs updated.
 *  \param kart_id The world kart id of the kart that was hit.
 *  \param hitter The world kart id of the kart who hit(-1 if none).
 */
void FreeForAllServer::handleKartHitInServer(int kart_id, int hitter)
{
    // WE WANT TO KEEP THE KART IN  GAME EVEN WHEN EXPLODED
    // SO IF NOT ENABLED, JUST SKIP SCORE UPDATE
    if (!m_karts[kart_id]->isEnabled())
        return;

    bool isKilled = false;
    AbstractKart* kart = getKart(kart_id);
    if(m_eliminated_karts < getNumKarts() - 1){

        // ONLY CHECKING SPEED.
        // WE NEED TO COMPARE VECTORS IN THE FUTURE
        // YOU CAN TAKE DAMAGE FROM THE SIDE OF A CAR SPEEDING

        if (hitter != -1){

            const KartProperties* kt = m_karts[kart_id]->getKartProperties();
            std::string type = kt->getKartType();
            float damage = 0;

            // WE ARE CALCULATING THE DAMAGE TO GIVE TO THE HITTER AND
            // THE KART HIT. WE WANT TO CHECK IF THE KART IS LIGHT,MED,
            // OR HEAVY FIRST. BASED ON THIS THE KART WILL RECEIVE A BONUS
            // ON THE HIT DAMAGE.

            // WE CHECK FIRST IF THE HITTER HAS SPEED > 0 THEN BASED ON THIS
            // WE WANT TO ASSIGN THE DAMAGE. IF TRUE -> DAMAGE = SPEED * KART SIZE BONUS
            // ELSE KART WILL RECEIVE DAMAGE BASED ON HIS SPEED AND THE CLASS OF THE
            // TARGET HIT KART (STATIONARY).
            // USEFUL SO THAT ALSO THE STATIONARY KART CAN GIVE BACK
            // SOME DAMAGE.

            // damage = speed * multiplier;m_karts[kart_id]->getSpeed()
            float speedHitter = m_karts[hitter]->getSpeed() < 0 ?
                                    m_karts[hitter]->getSpeed() * -1
                                    : m_karts[hitter]->getSpeed();
            float speedKartHit = m_karts[kart_id]->getSpeed() < 0 ?
                                    m_karts[kart_id]->getSpeed()* -1 :
                                    m_karts[kart_id]->getSpeed();
            if (type == "light"){
                damage = speedHitter != 0 ?
                             (speedHitter * 1.2) :
                             speedKartHit * 0.2;
            }
            else if (type == "medium"){
                damage = speedHitter != 0 ?
                             (speedHitter * 1.4) :
                             speedKartHit * 0.4;
            }
            else if (type == "heavy"){
                damage = speedHitter != 0 ?
                             (speedHitter * 1.6) :
                             speedKartHit * 0.6;
            }
            m_karts[kart_id]->doDamage(damage);
        }

        else if (kart_id == hitter || hitter == -1){
            isKilled = true;
        }

        if (isKilled == true || kart->getHp() <= 0)
        {
            m_karts[kart_id]->setHp(0);
            m_karts[kart_id]->finishedRace(WorldStatus::getTime());
            m_last_player_time = WorldStatus::getTime();
            eliminateKart(kart_id, false);
        }
    }
    calculateScore(kart_id);

}   // handleKartHitInServer

// ----------------------------------------------------------------------------
/** Called when the kart needs to be disconnected because inactive.
 *  \param kart_id The world kart id of the kart that was hit.
 */
void FreeForAllServer::handleDisconnectInServer(int kart_id){
    if (!m_karts[kart_id]->isEnabled()){
        return;
    }

    m_karts[kart_id]->setHp(0);
    m_karts[kart_id]->finishedRace(0.0f);
    m_last_player_time = WorldStatus::getTime();
    eliminateKart(kart_id, false);

    calculateScore(kart_id);
}
// ----------------------------------------------------------------------------
void FreeForAllServer::calculateScore(int kart_id){
    float finishTime;
    // LAST KART
    if(m_eliminated_karts == getNumKarts() - 1 && m_karts[kart_id]->isEnabled()){
        finishTime = m_last_player_time;
        m_karts[kart_id]->finishedRace(finishTime);
    }

    else{
        finishTime = getKart(kart_id)->getFinishTime();
    }

    // SET THE SERVER SCORE
    if(finishTime == RaceManager::get()->getTimeTarget()){
        m_scores.at(kart_id) = finishTime + getKart(kart_id)->getHp();
    }
    else if (finishTime == 0){
        m_scores.at(kart_id) = getKart(kart_id)->getHp();
    }
    else {
        m_scores.at(kart_id) = (RaceManager::get()->getTimeTarget() - finishTime)
                               + getKart(kart_id)->getHp();
    }

    if ((NetworkConfig::get()->isNetworking() &&
         NetworkConfig::get()->isServer()))
    {
        NetworkString p(PROTOCOL_GAME_EVENTS);
        p.setSynchronous(true);
        p.addUInt8(GameEventsProtocol::GE_TITLE_KART_SET);
        p.addUInt8((uint8_t)kart_id).addFloat((float_t)m_karts[kart_id]->getHp()).addFloat((float_t)m_scores.at(kart_id));
        STKHost::get()->sendPacketToAllPeers(&p, true);
    }
}

// ----------------------------------------------------------------------------
/** Returns the internal identifier for this race.
 */
const std::string& FreeForAllServer::getIdent() const
{
    return IDENT_FFA;
}   // getIdent

// ------------------------------------------------------------------------
void FreeForAllServer::update(int ticks)
{
    WorldWithRank::update(ticks);
    WorldWithRank::updateTrack(ticks);
    if (Track::getCurrentTrack()->hasNavMesh())
        updateSectorForKarts();
}   // update

// ----------------------------------------------------------------------------
/** The battle is over if only one kart is left, or no player kart.
 */
bool FreeForAllServer::isRaceOver()
{
    if (NetworkConfig::get()->isNetworking() &&
            NetworkConfig::get()->isClient())
        return false;

    if (!getKartAtPosition(1))
        return false;

    const int top_id = getKartAtPosition(1)->getWorldKartId();
    const int hit_capture_limit = RaceManager::get()->getHitCaptureLimit();
    if ((m_count_down_reached_zero && RaceManager::get()->hasTimeTarget()) ||
        (hit_capture_limit != 0 && m_scores[top_id] >= hit_capture_limit) ||
        (m_eliminated_karts == getNumKarts() - 1) || getCurrentNumPlayers() <= 1)
    {
        for (unsigned int i = 0; i < getNumKarts(); i++){
            if(getKart(i)->isEnabled()){
                calculateScore(getKart(i)->getWorldKartId());
            }
        }
        return true;
    }
    return false;

}   // isRaceOver

// ----------------------------------------------------------------------------
/** Returns the data to display in the race gui.
 */
void FreeForAllServer::getKartsDisplayInfo(
        std::vector<RaceGUIBase::KartIconDisplayInfo> *info)
{
    const unsigned int kart_amount = getNumKarts();
    for (unsigned int i = 0; i < kart_amount ; i++)
    {
        RaceGUIBase::KartIconDisplayInfo& rank_info = (*info)[i];
        rank_info.lap = -1;
        rank_info.m_outlined_font = true;
        rank_info.m_color = getColor(i);
        rank_info.m_text = getKart(i)->getController()->getName();
        if (RaceManager::get()->getKartGlobalPlayerId(i) > -1)
        {
            const core::stringw& flag = StringUtils::getCountryFlag(
                        RaceManager::get()->getKartInfo(i).getCountryCode());
            if (!flag.empty())
            {
                rank_info.m_text += L" ";
                rank_info.m_text += flag;
            }
        }
        rank_info.m_text += core::stringw(L" (") +
                StringUtils::toWString(getKart(i)->getHp()) + L")";

    }
}   // getKartsDisplayInfo
// ----------------------------------------------------------------------------
    void FreeForAllServer::terminateRace()
    {
        const unsigned int kart_amount = getNumKarts();
        for (unsigned int i = 0; i < kart_amount ; i++)
            {
                getKart(i)->finishedRace(0.0f, true/*from_server*/);
            }   // i<kart_amount
        FreeForAll::terminateRace();
    }   // terminateRace

// ----------------------------------------------------------------------------

video::SColor FreeForAllServer::getColor(unsigned int kart_id) const
{
    return GUIEngine::getSkin()->getColor("font::normal");
}   // getColor

// ----------------------------------------------------------------------------
bool FreeForAllServer::getKartFFAResult(int kart_id)
{
    // the kart(s) which has the top score wins
    AbstractKart* k = getKartAtPosition(1);
    if (!k)
        return false;
    int top_score = getKartScore(k->getWorldKartId());
    return getKartScore(kart_id) == top_score;
}   // getKartFFAResult

// ----------------------------------------------------------------------------
void FreeForAllServer::saveCompleteState(BareNetworkString* bns, STKPeer* peer)
{
    for (unsigned i = 0; i < m_scores.size(); i++)
        bns->addUInt32(m_scores[i]);
}   // saveCompleteState

// ----------------------------------------------------------------------------
void FreeForAllServer::restoreCompleteState(const BareNetworkString& b)
{
    for (unsigned i = 0; i < m_scores.size(); i++)
        m_scores[i] = b.getUInt32();
}   // restoreCompleteState

void FreeForAllServer::eliminateKart(int kart_number, bool notify_of_elimination)
{
    assert(kart_number < (int)m_karts.size());
    AbstractKart *kart = m_karts[kart_number].get();

    if (kart->isGhostKart()) return;

    // Display a message about the eliminated kart in the race gui
    if (m_race_gui && notify_of_elimination)
    {

        for(unsigned int i=0; i<Camera::getNumCameras(); i++)
        {
            Camera *camera = Camera::getCamera(i);
            if(camera->getKart()==kart)
                m_race_gui->addMessage(_("You have been eliminated!"), kart,
                                       2.0f);
            else
            {
                // Store the temporary string because clang would mess this up
                // (remove the stringw before the wchar_t* is used).
                const core::stringw &kart_name = kart->getController()->getName();
                m_race_gui->addMessage(_("'%s' has been eliminated.",
                                         kart_name),
                                       camera->getKart(),
                                       2.0f);
            }
        }  // for i < number of cameras
    }   // if notify_of_elimination

    //    // The kart can't be really removed from the m_kart array, since otherwise
    //    // a race can't be restarted. So it's only marked to be eliminated (and
    //    // ignored in all loops). Important:world->getCurrentNumKarts() returns
    //    // the number of karts still racing. This value can not be used for loops
    //    // over all karts, use RaceManager::get()->getNumKarts() instead!
    kart->explodeKart();
    m_eliminated_karts++;

}   // eliminateKart


// ----------------------------------------------------------------------------
std::pair<uint32_t, uint32_t> FreeForAllServer::getGameStartedProgress() const
{
    std::pair<uint32_t, uint32_t> progress(
                std::numeric_limits<uint32_t>::max(),
                std::numeric_limits<uint32_t>::max());
    if (RaceManager::get()->hasTimeTarget())
    {
        progress.first = (uint32_t)m_time;
    }
    AbstractKart* k = getKartAtPosition(1);
    float score = -1.0f;
    if (k)
        score = (float)getKartScore(k->getWorldKartId());

    if (score >= 0.0f)
    {
        progress.second = (uint32_t)(score /
                                     (float)RaceManager::get()->getHitCaptureLimit() * 100.0f);
    }
    return progress;
}   // getGameStartedProgress
