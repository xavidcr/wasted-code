#!/bin/bash

i=1

#set a default terminal to open external windows
terminal="xterm"

# Main Menu

function main (){

    var=$(zenity --list --column="" i "Start" i "Compile" i "Debug Menu" i "Exit" --column=Options --text="STK Helper"  --radiolist)

    if [[ $var == "Compile" ]]
    then

	zenity --info --text="I will ask your password to install the dependencies"
	zenity --password | sudo -S apt-get install build-essential cmake libbluetooth-dev libsdl2-dev libcurl4-openssl-dev libenet-dev libfreetype6-dev libharfbuzz-dev libjpeg-dev libogg-dev libopenal-dev libpng-dev libssl-dev libvorbis-dev libmbedtls-dev pkg-config zlib1g-dev

	mkdir cmake_build
	cd cmake_build
	cmake ..
	zenity --info --text="Starting to compile"    
	make -j5 | zenity --progress --text="COMPILING..." --pulsate --time-remaining --auto-close
	cd ..
	zenity --info --text="Done, now you can run the game!"


    elif [[ $var == "Start" ]]
    then
	./cmake_build/bin/supertuxkart	


    elif [[ $var == "Debug Menu" ]]
    then
	debug_menu	


    elif [[ $var == "Exit" ]]

    then
	echo "bye!"
	i=-1;
	
	
    fi

}

function debug_menu {

    var=$(zenity --list --column="" i "Launch Server (will open another window)" i "Connect"  i "Back" --column=Options --text="Debug Menu"  --radiolist)
    cd cmake_build/bin/
    if [[ $var == "Launch Server (will open another window)" ]]
    then	
	$terminal -e "./supertuxkart --server-config=your_config.xml --network-console" & disown; 

    elif [[ $var == "Connect" ]]
    then
	./supertuxkart --connect-now=127.0.0.1:2759 &
	

    elif [[ $var == "Back" ]]
    then
	main	

    fi
}

# Main Loop
# If i has been set to -1 by "exit" then... break

while [ $i == 1 ]
do
    main
    
done

